# EP1 - OO 2019.2 (UnB - Gama)

Turmas Renato e Carla
Data de entrega: 01/10/2019

### Modo venda
- Ao entrar no modo venda o programa solicita o cpf do cliente para verificar se o mesmo eh socio ou nao
- Caso ele nao seja socio, abre-se uma tela de cadastro pedindo o nome, a data de nascimento e o endereco 
- Caso ele ja seja socio aparece uma tela com todos os produtos ja cadastrados em lista e aparece o valor do desconto

### Modo estoque
- Ao entrar no modo estoque pergunta-se o o ID do produto para verificar se o mesmo ja esta cadastrado ou nao
- Caso nao esteja cadastrado, abre-se uma tela de cadastro solicitando as informacoes: nome, quantidade, preco
- Caso o produto esteja cadastrado ele eh listado

## Orientações

- Para compilar o projeto, primeiro precisa-se clonar ele do git lab no terminal
- Apos devera isso deve-se dar o make no programa e se rodar, dar make run.
- Logo em seguida seu programa devera funcionar para a execucao. 
