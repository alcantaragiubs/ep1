#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class classeProduto {
private:
	int id;
	string nome;
	string categoria;
	int quantidade;
	float preco;
public:
	static int qtdProdutos;
	void setId(int id);
	int getId();
	void setNome(string nome);
	string getNome();
	void setCategoria(string categoria);
	string getCategoria();
	void setQuantidade(int quantidade);
	int getQuantidade();
	void setPreco(float preco);
	float getPreco();
	classeProduto();
	classeProduto(int id, string nome, string categoria, int quantidade, float preco);
	classeProduto(int id);
	friend ostream& operator << (ostream& out, const classeProduto& produto);
	friend istream& operator >> (istream& in, classeProduto& produto);
	static void gravaProduto(classeProduto produto);
	static classeProduto existeProduto(int id);
	static void lerProdutos(classeProduto produtos[50]);
};

