#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "classePessoa.h"
using namespace std;

class classeCliente : public classePessoa {
private:
	int idCliente;
public:
	classeCliente(string cpf, string dataNascimento, string nomeCompleto, string endereco) : classePessoa(cpf, dataNascimento, nomeCompleto, endereco) {
	}
	classeCliente(string cpf) : classePessoa(cpf) {
	}
	void setId(int idCliente);
	int getId();
	friend ostream& operator << (ostream& out, const classeCliente& cliente);
	friend istream& operator >> (istream& in, classeCliente& cliente);
	static void gravaCliente(classeCliente cliente);
	static int existeCliente(string cpf);
};


