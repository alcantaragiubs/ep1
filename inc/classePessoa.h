#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class classePessoa {
protected:
	string cpf;
	string dataNascimento;
	string nomeCompleto;
	string endereco;
public:
	void setCPF(string cpf);
	string getCPF();
	void setData(string dataNascimento);
	string getData();
	void setNome(string nomeCompleto);
	string getNome();
	void setEndereco(string endereco);
	string getEndereco();
	classePessoa(string cpf, string dataNascimento, string nomeCompleto, string endereco);
	classePessoa(string cpf);
};

