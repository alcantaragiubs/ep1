#include "classePessoa.h"
void classePessoa::setCPF(string cpf) {
	this->cpf = cpf;
}
string classePessoa::getCPF() {
	return cpf;
}
void classePessoa::setData(string dataNascimento) {
	this->dataNascimento = dataNascimento;
}
string classePessoa::getData() {
	return dataNascimento;
}
void classePessoa::setNome(string nomeCompleto) {
	this->nomeCompleto = nomeCompleto;
}
string classePessoa::getNome() {
	return nomeCompleto;
}
void classePessoa::setEndereco(string endereco) {
	this->endereco = endereco;
}
string classePessoa::getEndereco() {
	return endereco;
}
classePessoa::classePessoa(string cpf, string dataNascimento, string nomeCompleto, string endereco) {
	this->cpf = cpf;
	this->dataNascimento = dataNascimento;
	this->nomeCompleto = nomeCompleto;
	this->endereco = endereco;
}
classePessoa::classePessoa(string cpf) {
	this->cpf = cpf;
}