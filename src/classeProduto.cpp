#include "classeProduto.h"
using namespace std;

void classeProduto::setId(int id) {
	this->id = id;
}
int classeProduto::getId() {
	return id;
}
void classeProduto::setNome(string nome) {
	this->nome = nome;
}
string classeProduto::getNome() {
	return nome;
}
void classeProduto::setCategoria(string categoria) {
	this->categoria = categoria;
}
string classeProduto::getCategoria() {
	return categoria;
}
void classeProduto::setQuantidade(int quantidade) {
	this->quantidade = quantidade;
}
int classeProduto::getQuantidade() {
	return quantidade;
}
void classeProduto::setPreco(float preco) {
	this->preco = preco;
}
float classeProduto::getPreco() {
	return preco;
}
classeProduto::classeProduto(int id, string nome, string categoria, int quantidade, float preco) {
	this->id = id;
	this->nome = nome;
	this->categoria = categoria;
	this->quantidade = quantidade;
	this->preco = preco;
}
classeProduto::classeProduto() {
	this->id = 0;
	this->preco = 0;
	this->quantidade = 0;
}
std::ostream& operator << (ostream& out, const classeProduto& produto) {
	out << produto.id << "\n" << produto.categoria << "\n" << produto.nome << "\n" << produto.preco << endl;
	return out;
}
std::istream& operator >> (istream& in, classeProduto& produto) {
	in >> produto.id;
	in >> produto.categoria;
	in >> produto.nome;
	in >> produto.preco;
	return in;
}
 void classeProduto::gravaProduto(classeProduto produto) {
	ofstream arqProduto;
	arqProduto.open("produto.txt", ios::out | ios::app);
	if (arqProduto.is_open()) {
		arqProduto << produto;
	}
	arqProduto.close();
}
 classeProduto classeProduto::existeProduto(int id) {
	 ifstream arqProduto;
	 classeProduto produto;
	 arqProduto.open("produto.txt", ios::in | ios::app);
	 if (arqProduto.is_open()) {
		 while (arqProduto >> produto) {
			 if (produto.getId() == id) {
				 arqProduto.close();
				 return produto;
			 }
		 }
	 }
	 arqProduto.close();
	 produto.id = 0;
	 return produto;
 }
 void classeProduto::lerProdutos(classeProduto produtos[50]) {
	 classeProduto produto;
	 ifstream arqProduto;
	 arqProduto.open("produto.txt", ios::in | ios::app);
	 if (arqProduto.is_open()) {
		 while (arqProduto >> produto) {
			 produtos[qtdProdutos] = produto;
			 qtdProdutos++;
		 }
		 arqProduto.close();
	 }
 }
int classeProduto::qtdProdutos = 0;
