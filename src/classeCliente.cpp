#include "classeCliente.h"
using namespace std;

	
	void classeCliente::setId(int idCliente) {
		this->idCliente = idCliente;
	}
	int classeCliente::getId() {
		return idCliente;
	}
	

	std::ostream &operator << (ostream &out, const classeCliente & cliente) {
		out << cliente.cpf << "\n" << cliente.dataNascimento << "\n" << cliente.nomeCompleto << "\n" << cliente.endereco << endl;
		return out;
	}
	std::istream& operator >> (istream& in, classeCliente& cliente) {
		in >> cliente.cpf;
		in >> cliente.dataNascimento;
		in >> cliente.nomeCompleto;
		in >> cliente.endereco;
		return in;
	}
	void classeCliente::gravaCliente(classeCliente cliente) {
		ofstream arqCliente;
		arqCliente.open("cliente.txt", ios::out | ios::app);
		if (arqCliente.is_open()) {
			arqCliente << cliente;
		}
		arqCliente.close();
	}
	int classeCliente::existeCliente(string cpf) {
		classeCliente cliente(cpf);
		ifstream in("cliente.txt");
		if (in.is_open()) {
			while (in >> cliente) {
				if (cliente.getCPF().compare(cpf) != 0) {
					return 1;
				}
			}
		}
		in.close();
		return 0;
	}
