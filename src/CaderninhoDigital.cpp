#include <cstdlib>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include "classeCliente.h" 
#include "classeProduto.h"
#include "classePessoa.h"
using namespace std;

//prototipos
void gotoxy(int x, int y);
void flush_in();
void menuPrincipal();
void modoVenda();
void cadastraCliente(string cpf); 
void modoEstoque();
void cadastraProduto(int id);
void listaProdutos();
void carrinhoCompras();

void padraoTela(string funcaoSistema);
void mensagemTelaComFlush(string mensagem);
void mensagemTelaSemFlush(string mensagem);
//void validaNumeroLegenda(FILE* arquivo_cliente, int* numeroLegenda);
//void validaNome(char nome[MAXNOME]);
//int nomeComCaracterValido(char nome[MAXNOME]);
//void ordenaNomeCrescente(struct cadastroCandidato* candidatos, int* totalCandidatos);
//void subMenu();
//void maiuscula(char nome[MAXNOME]);
char* trim(char* str);

//************************************** PROGRAMA PRINCIPAL **************************************

//Sintese
//Objetivo: registrar os candidatos a prefeito de uma cidade brasileira para as elei��es que acontecer�o em 2020
//Entrada: Nome, numero de inscri��o, sigla do partido do candidato se o candidato esta na lava-jato ou nao
//Saida: lista de todos os candidatos cadastrados, 
int main() {
	//system("color 07");
	//classeCliente cliente();
	string mensagemRetorno = "Opcao Invalida!";
	int opcao, totalCandidatos = 0;
	
	menuPrincipal();
	gotoxy(69, 19);
	cin >> opcao;
	while (opcao != 0) {
		switch (opcao)
		{
		case 1:
			modoVenda();
			break;
			
		case 2:
			modoEstoque();
			break;
		case 3:
			//modoRecomendacao();
			break;
		default:
			mensagemTelaComFlush(mensagemRetorno);
		}
		menuPrincipal();
		gotoxy(69, 19); 
		cout << "                                           ";
		gotoxy(69, 19);
		cin >> opcao;
	}
	return 0;
}

//************************************** SUBPROGRAMAS **************************************

//Sintese:
//Objetivo: Criar um menu que apresente as op��es do que o usuario pode fazer no programa 
//Parametro: nenhum
//Saida: nenhuma
void menuPrincipal() {
	string funcaoSistema = "MENU";
	system("clear");
	padraoTela(funcaoSistema);
	gotoxy(41, 12);
	cout << "1 - Modo Venda" << endl;
	gotoxy(41, 13);
	cout << "2 - Modo Estoque" << endl;
	gotoxy(41, 14);
	cout << "3 - Modo Recomendacao" << endl;
	gotoxy(41, 15);
	cout << "0 - Sair do caderno" << endl;
	cout << endl;
	gotoxy(41, 19);
	cout << "Selecione a opcao desejada: " << endl;
}
void modoVenda() {
	string funcaoSistema = "MODO VENDA", cpf, mensagemRetorno = "Cliente Existente!";
	//int id, quantidade;
	system("clear");
	padraoTela(funcaoSistema);
	gotoxy(0, 3);
	cout << "CPF: ";
	cin >> cpf;
	//classeCliente cliente(cpf);
	//cliente.existeCliente(cpf, cliente);
	classePessoa pessoa(cpf);
	if(classeCliente::existeCliente(cpf)) {
		mensagemTelaComFlush(mensagemRetorno);
		carrinhoCompras();
	}
	else { 
		cadastraCliente(cpf);
	}
}
void carrinhoCompras() {
	string funcaoSistema = "CARRINHO DE COMPRAS", mensagemRetorno = "Compra Finalizada!";
	int id, quantidade;
	classeProduto produto;
	char opcao = 's';
	int aux;
	system("clear");
	padraoTela(funcaoSistema);
	//listaProdutos();
	//gotoxy(26, 0);
	while (opcao == 's') {
		cout << "ID do produto: ";
		gotoxy(0, 4);
		cout << "Quantidade: " << endl;
		gotoxy(15, 2);
		cin >> id;
		produto = classeProduto::existeProduto(id);
		if (produto.getId() != 0) {
			id++;
		}
		gotoxy(12, 4);
		cin >> quantidade;
		quantidade++;
		gotoxy(0, 6);
		cout << "Deseja comprar mais algum item? ";
		cin >> opcao;
		system("clear");
		padraoTela(funcaoSistema);
	}
	cout << id;
	cout << quantidade;
	for (aux = 0; aux < 120; aux++) {
		cout << "-";
	}
	//gotoxy para saber a linha vem aqui
	cout << "Valor Total: " << endl;
	cout << endl;
	cout << " - Desconto: 15%" << endl;
	cout << endl;
	cout << "Valor final: " << endl;
	cout << "Deseja finalizar o pedido? " << endl;
	cin >> opcao;
	if (opcao == 'n') {
		//voltar ao inicio da fun�ao
	}
	else {
		// se quant de produtos escolhida > quant de produtos existente
		// cout << "Compra Cancelada por falta de estoque! Deseja come�ar outro pedido?
		// cin >> opcao;
		// se quant de produtos escolhida < quant de produtos existente
		//	mensagemTelaComFlush(mensagemRetorno);
	}
}
//Sintese:
//Objetivo: Cadastrar os dados dos clientes
//Parametro: arquivo
//Saida: nenhuma
void cadastraCliente(string cpf) {
	string nome, endereco, data_de_nascimento, funcaoSistema = "CADASTRO DE CLIENTES", mensagemRetorno = "Dados Cadastrados!";
	system("clear");
	padraoTela(funcaoSistema);
	cout << "CPF: " << cpf << endl;
	cout << endl;
	cout << "Nome: " << endl;
	cout << endl;
	cout << "Data de Nascimento: " << endl;
	cout << endl;
	cout << "Endereco: " << endl;
	gotoxy(6, 4);
	cin >> nome;
	gotoxy(20, 6);
	cin >> data_de_nascimento;
	gotoxy(10, 8);
	cin >> endereco;
	classeCliente cliente(cpf, nome, data_de_nascimento, endereco);
	cliente.gravaCliente(cliente);
	gotoxy(28, 26);
	mensagemTelaComFlush(mensagemRetorno);
	listaProdutos();
	}
void modoEstoque() {
	string funcaoSistema = "MODO ESTOQUE";
	int id;
	system("clear");
	padraoTela(funcaoSistema);
	gotoxy(0, 4);
	cout << "ID: ";
	cin >> id;
	//classeProduto produto(id);
	if(classeProduto::existeProduto(id).getId() != 0) {
		listaProdutos();
	}
	else { 
	cadastraProduto(id);
	}
}
void cadastraProduto(int id) {
	string categoria, nome, funcaoSistema = "CADASTRO DE PRODUTOS", mensagemRetorno = "Produto Cadastrado!";
	int quantidade;
	float preco;
	system("clear");
	padraoTela(funcaoSistema);
	cout << "ID: " << id << endl;
	cout << endl;
	cout << "Categoria: " << endl;
	cout << endl;
	cout << "Nome do Produto: " << endl;
	cout << endl;
	cout << "Quantidade: " << endl;
	cout << endl;
	cout << "Preco: " << endl;
	gotoxy(11, 4);
	cin >> categoria;
	gotoxy(17, 6);
	cin >> nome;
	gotoxy(12, 8);
	cin >> quantidade;
	gotoxy(7, 10);
	cin >> preco;
	classeProduto produto(id, categoria, nome, quantidade, preco);
	produto.gravaProduto(produto);
	gotoxy(28, 26);
	mensagemTelaComFlush(mensagemRetorno);
	listaProdutos();
}
void listaProdutos() {
	string nome, categoria, funcaoSistema = "LISTA PRODUTOS", mensagemRetorno = " ";
	char opcao = 'c';
	//int id, quantidade, , 
	int linha, aux;
	//float preco;
	//classeProduto produto(id, categoria, nome, quantidade, preco);
	classeProduto produtos[50];
	classeProduto::lerProdutos(produtos);
	while (opcao != '0') {
		linha = 5;
		system("clear");
		padraoTela(funcaoSistema);
		gotoxy(15, 3);
		cout << "ID          Categoria          Nome          Quantidade          Preco" << endl;
		for (aux = 0; aux < 120; aux++) {
			cout << "-";
		}
		//if (opcao == 'c' || opcao == 'C') {
			//ordenaNomeCrescente(candidatos, &totalCandidatos);
		//}
		for (aux = 0; aux < classeProduto::qtdProdutos; aux++) {
			gotoxy(15, linha);
			cout << produtos->getId();
			gotoxy(27, linha);
			cout << produtos->getCategoria();
			gotoxy(46, linha);
			cout << produtos->getNome();
			gotoxy(60, linha);
			cout << produtos->getQuantidade();
			gotoxy(80, linha);
			cout << produtos->getPreco();
			linha++;
		}
		gotoxy(30, 26);
		cout <<"Informe a opcao desejada: (C)rescente ou 0 (Voltar ao Menu) ";
		cin >> opcao;
	}
}
/**/

/*int nomeComCaracterValido(char* nome) {
	int aux, temCaracterValido = 1;
	char c;
	for (aux = 0; aux <= strlen(nome) - 1; aux++) {
		c = nome[aux];
		if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z') && !(c == ' ')) {
			temCaracterValido = 0;
			break;
		}
	}
	return temCaracterValido;
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
int siglaComCaracterValido(char *sigla) {
	int aux, temCaracterValido = 1;
	char c;
	for (aux = 0; aux <= strlen(sigla) - 1; aux++) {
		c = sigla[aux];
		if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z') && (c == ' ')) {
			temCaracterValido = 0;
			break;
		}
	}
	return temCaracterValido;
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
//void ordenaNomeCrescente(struct cadastroCandidato* candidatos, int* totalCandidatos) {
//	struct cadastroCandidato candidato, troca;
//	char nome1[MAXNOME], nome2[MAXNOME];
//	int aux, indice;
//	for (aux = 0; aux < *totalCandidatos; aux++) {
//		for (indice = aux + 1; indice < *totalCandidatos; indice++) {
//			strcpy(nome1, candidatos[aux].nome);
//			strcpy(nome2, candidatos[indice].nome);
//			if (strcmp(nome1, nome2) > 0) {
//				troca = candidatos[indice];
//				candidatos[indice] = candidatos[aux];
//				candidatos[aux] = troca;
//				//strcpy(candidatos[aux].nome, nome1);
//				//strcpy(candidatos[indice].nome, nome2);
//			}
//		}
//	}
//}
void maiuscula(char* nome) {
	int aux, tam;
	tam = strlen(nome);
	for (aux = 0; aux < tam; aux++) {
		nome[aux] = toupper(nome[aux]);
	}
}
//************************************** VALIDACAO ******************************************************

//Sintese
//Objetivo:
//Entrada:
//Saida:
/*void validaNumeroLegenda(FILE* arquivo_cliente, int* numeroLegenda) {
	struct cadastroCandidato candidato;
	char mensagemRetorno[MAXMENSAGEM] = "NUMERO INVALIDO OU NUMERO JA EXISTENTE!";
	int numeroNaoOk = 1;
	while (numeroNaoOk) {
		if ((*numeroLegenda >= 10 && *numeroLegenda <= 100) && (!existeCandidato(arquivo_cliente, numeroLegenda, &candidato))) {
			numeroNaoOk = 0;
		}
		else {
			mensagemTelaComFlush(mensagemRetorno);
			gotoxy(19, 3);
			printf("                                           ");
			gotoxy(19, 3);
			scanf("%d", numeroLegenda);
		}
	}
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
void validaNome(char* nome) {
	char mensagemRetorno[MAXMENSAGEM] = "NOME INVALIDO!";
	*nome = *trim(nome);
	while ((strlen(nome) < 1) || (!nomeComCaracterValido(nome))) {
		mensagemTelaSemFlush(mensagemRetorno);
		gotoxy(6, 5);
		printf("                                           ");
		gotoxy(6, 5);
		fgets(nome, MAXNOME, stdin);
		if (nome[strlen(nome) - 1] == '\n')
			nome[strlen(nome) - 1] = '\0';
		*nome = *trim(nome);
	}
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
*/

//************************************** SUBPROGRAMAS AUXILIADORES **************************************
void gotoxy(int x, int y)
{
	//COORD c = { x, y };
	//SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	printf("%c[%d;%df",0x1B,y,x);
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
void flush_in() {
	int ch;
	while ((ch = fgetc(stdin)) != EOF && ch != '\n') {}
};
//Sintese:
//Objetivo: Padronizar todas as telas do sistema
//Parametro: palavra?
//Saida: nenhuma
void padraoTela(string funcaoSistema) {
	int aux;
	gotoxy(0, 0);
	cout << funcaoSistema << endl;
	gotoxy(39, 0);
	cout << "CADERNINHO DIGITAL - VICTORIA" << endl;
	gotoxy(109, 0);
	cout << "UNB - FGA" << endl;
	for (aux = 0; aux < 120; aux++) {
		cout << "-" ;
	}
	gotoxy(0, 25);
	for (aux = 0; aux < 120; aux++) {
		cout << "-";
	}
	gotoxy(0, 2);
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
void mensagemTelaComFlush(string mensagem) {
	char tecla;
	int aux;
	gotoxy(0, 26);
	flush_in();
	cout << mensagem << " Tecle ENTER para continuar...";
	cin >> tecla;
	//getchar();
	gotoxy(0, 26);
	for (aux = 0; aux < 120; aux++) {
		cout << " ";
	}
	//flush_in();
}
//Sintese
//Objetivo:
//Entrada:
//Saida:
void mensagemTelaSemFlush(string mensagem) {
	char tecla;
	int aux;
	gotoxy(0, 26);
	//flush_in();
	cout << " Tecle ENTER para continuar...";
	cin >> tecla;
	//_getch();
	gotoxy(0, 26);
	for (aux = 0; aux < 120; aux++) {
		cout << " ";
	}
	//flush_in();
}

